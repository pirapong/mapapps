import 'package:flutter/material.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Map extends StatefulWidget {
  @override
  _MapState createState() => _MapState();
}

class _MapState extends State<Map> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('location picker'),
        ),
        body: Stack(
          children: [
            FlatButton(
              onPressed: () async {
                LocationResult result = await showLocationPicker(
                  context, 'AIzaSyB0m12-3Ra5JwzD25GCs_JumhoYGgB9cRM',
                  initialCenter: LatLng(13.816397, 100.515626),
                  automaticallyAnimateToCurrentLocation: true,
//                      mapStylePath: 'assets/mapStyle.json',
                  myLocationButtonEnabled: true,
                  requiredGPS: true,
                  layersButtonEnabled: true,
                  // countries: ['AE', 'NG']

//                      resultCardAlignment: Alignment.bottomCenter,
                  // desiredAccuracy: LocationAccuracy.best,
                );
                print("result = $result");
                // setState(() => _pickedLocation = result);
              },
              child: Text('Pick location'),
            ),
          ],
        ));
  }
}
